import PropTypes from 'prop-types'
import { useDispatch } from 'react-redux'
import { useNavigate } from 'react-router'
import conditioning from '../../../../images/tickets/tickets-conditioning.svg'
import express from '../../../../images/tickets/tickets-express.svg'
import food from '../../../../images/tickets/tickets-food.svg'
import rub from '../../../../images/tickets/tickets-rub.svg'
import wifi from '../../../../images/tickets/tickets-wifi.svg'

import { passengersPriceClear } from '../../../../slices/passengersSlice'
import { coachItemsClear, trainAdd } from '../../../../slices/seatsSlice'

export default function LastTicket({ ticket }) {
  const dispatch = useDispatch()
  const navigate = useNavigate()

  const onClick = () => {
    dispatch(trainAdd({ train: ticket }))
    dispatch(coachItemsClear({ type: 'arrival' }))
    dispatch(coachItemsClear({ type: 'departure' }))
    dispatch(passengersPriceClear())
    navigate('/order/tickets/seats')
  }

  return (
    <div className='last_ticket' onClick={onClick}>
      <div className='last_ticket-header'>
        {Array.from({ length: 2 }).map((_, i) => (
          <div className={i === 0 ? 'last_ticket-title-from' : 'last_ticket-title-to'} key={i}>
            <h4 className='last_ticket-title'>
              {i === 0 ? ticket.departure.from.city.name : ticket.departure.to.city.name}
            </h4>

            <span className='last_ticket-subtitle'>
              {ticket.departure.from.railway_station_name}
              <br />
              вокзал
            </span>
          </div>
        ))}
      </div>

      <div className='last_ticket-info'>
        <div className='last_ticket-options'>
          {ticket.departure.have_wifi && (
            <img className='last_ticket-options-icon' src={wifi} alt='wi-fi' />
          )}

          {ticket.departure.is_express && (
            <img className='last_ticket-options-icon' src={express} alt='express' />
          )}

          {ticket.departure.have_air_conditioning && (
            <img className='last_ticket-options-icon' src={conditioning} alt='conditioning' />
          )}

          <img className='last_ticket-options-icon' src={food} alt='food' />
        </div>

        <div className='last_ticket-price-range'>
          <span className='last_ticket-price'>
            от{' '}
            <span className='last_ticket-price-value currency-item'>
              {ticket.departure.min_price.toLocaleString('ru')}
            </span>
            <img className='last_ticket__currency' src={rub} alt='руб.' />
          </span>
        </div>
      </div>
    </div>
  )
}

LastTicket.propTypes = {
  ticket: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.string, PropTypes.number, PropTypes.bool, PropTypes.object])
  ).isRequired,
}
