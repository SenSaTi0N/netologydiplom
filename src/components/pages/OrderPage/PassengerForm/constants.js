export const PERSON_NAME = [
  {
    id: 1,
    name: 'surname',
    placeholder: 'Фамилия',
  },
  {
    id: 2,
    name: 'name',
    placeholder: 'Имя',
  },
  {
    id: 3,
    name: 'lastname',
    placeholder: 'Отчество',
  },
]
