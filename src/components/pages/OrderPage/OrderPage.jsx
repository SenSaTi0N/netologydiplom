import { Routes } from 'react-router'
import { Route } from 'react-router-dom'
import Footer from '../../Footer/Footer'
import Seats from '../../Seats/Seats'
import SideBar from '../../SideBar/SideBar'
import './OrderPage.css'
import OrderPageHeader from './OrderPageHeader/OrderPageHeader'
import Passengers from './Passengers/Passengers'
import PayForm from './PayForm/PayForm'
import TrainSelector from './TrainSelector/TrainSelector'
import Verification from './Verification/Verification'

export default function OrderPage() {
  return (
    <div className='order-page'>
      <OrderPageHeader />

      <main className='order-page__content'>
        <div>
          <SideBar />
        </div>

        <Routes>
          <Route path='/tickets/train' element={<TrainSelector />} />
          <Route path='/tickets/seats' element={<Seats />} />
          <Route path='/passengers/' element={<Passengers />} />
          <Route path='/pay/' element={<PayForm />} />
          <Route path='/verification/' element={<Verification />} />
        </Routes>
      </main>

      <Footer />
    </div>
  )
}
