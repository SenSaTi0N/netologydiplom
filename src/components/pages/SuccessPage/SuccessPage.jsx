import Footer from '../../Footer/Footer'
import Logo from '../../Logo/Logo'
import Menu from '../../Menu/Menu'
import SuccessOrder from './SuccessOrder/SuccessOrder'
import './SuccessPage.css'

export default function SuccessPage() {
  return (
    <div className='success-page'>
      <div className='success-page-header'>
        <div className='logo-wrapper'>
          <Logo />
        </div>

        <div className='menu-wrapper'>
          <Menu />
        </div>
      </div>

      <SuccessOrder />
      <Footer />
    </div>
  )
}
