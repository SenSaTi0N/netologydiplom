import { Carousel } from 'react-responsive-carousel'
import 'react-responsive-carousel/lib/styles/carousel.min.css'
import './Reviews.css'

import Review from './Review'
import { REVIEWS } from './constants'

export default function Reviews() {
  return (
    <section id='reviews'>
      <h2 className='reviews__title'>Отзывы</h2>

      <Carousel infiniteLoop showStatus={false} showArrows={false} showThumbs={false}>
        {Array.from({ length: 5 }, (_, i) => (
          <div className='reviews' key={i}>
            {REVIEWS.map((item) => (
              <Review image={item.image} name={item.name} text={item.text} key={item.id} />
            ))}
          </div>
        ))}
      </Carousel>
    </section>
  )
}
