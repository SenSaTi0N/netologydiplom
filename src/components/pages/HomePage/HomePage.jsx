import Footer from '../../Footer/Footer'
import About from './About/About'
import HomePageHeader from './HomePageHeader/HomePageHeader'
import HowItWorks from './HowItWorks/HowItWorks'
import Reviews from './Reviews/Reviews'

export default function HomePage() {
  return (
    <div>
      <HomePageHeader />
      <About />
      <HowItWorks />
      <Reviews />
      <Footer />
    </div>
  )
}
