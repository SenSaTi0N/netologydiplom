import './About.css'
import { TEXT } from './constants'

export default function About() {
  return (
    <section className='about' id='about'>
      <div className='wrapper'>
        <h1 className='about__title'>О нас</h1>
        <div className='about__content'>
          {TEXT.map((item) => (
            <p className='about__text text_normal' key={item.id}>
              {item.title}
              <br />
              {item.subtitle}
            </p>
          ))}
        </div>
      </div>
    </section>
  )
}
