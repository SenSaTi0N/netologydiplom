import { Link } from 'react-router-dom'
import './HowItWorks.css'
import HowItWorksElement from './HowItWorksElement'
import { HOW_IT_WORKS_ELEMENTS } from './constants'

export default function HowItWorks() {
  return (
    <section className='how-it-works' id='how-it-works'>
      <div className='wrapper'>
        <div className='how-it-works__wrapper'>
          <h2 className='how-it-works__title'>Как это работает</h2>
          <Link className='how-it-works__learn-more'>Узнать больше</Link>
        </div>

        <div className='how-it-works__elements'>
          {HOW_IT_WORKS_ELEMENTS.map((item) => (
            <HowItWorksElement image={item.image} text={item.text} key={item.id} />
          ))}
        </div>
      </div>
    </section>
  )
}
