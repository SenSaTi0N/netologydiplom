export const HOW_IT_WORKS_ELEMENTS = [
  {
    id: 1,
    image: './src/images/icons/comfortable-order_icon.png',
    text: 'Удобный заказ на сайте',
  },
  {
    id: 2,
    image: './src/images/icons/out-of-office_icon.png',
    text: 'Нет необходимости ехать в офис',
  },
  {
    id: 3,
    image: './src/images/icons/a-lot-of-directions_icon.png',
    text: 'Огромный выбор направлений',
  },
]
