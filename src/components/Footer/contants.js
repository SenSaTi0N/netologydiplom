export const FOOTER_CONTACTS_ICONS = [
  {
    id: 1,
    name: 'phone',
    src: './src/images/icons/phone.png',
    text: '8 (800) 000 00 00',
  },
  {
    id: 2,
    name: 'email',
    src: './src/images/icons/email.png',
    text: 'inbox@mail.ru',
  },
  {
    id: 3,
    name: 'skype',
    src: './src/images/icons/skype.png',
    text: 'tu.train.tickets',
  },
  {
    id: 4,
    name: 'geo',
    src: './src/images/icons/geo.png',
  },
]

export const FOOTER_SUBSCRIBE_ICONS = [
  {
    id: 1,
    name: 'Youtube',
    src: './src/images/icons/youtube.png',
  },
  {
    id: 2,
    name: 'LinkedIn',
    src: './src/images/icons/linkedin.png',
  },
  {
    id: 3,
    name: 'Google',
    src: './src/images/icons/google.png',
  },
  {
    id: 4,
    name: 'Facebook',
    src: './src/images/icons/facebook.png',
  },
  {
    id: 5,
    name: 'Twitter',
    src: './src/images/icons/twitter.png',
  },
]
