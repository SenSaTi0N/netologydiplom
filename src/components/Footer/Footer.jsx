import './Footer.css'

import upArrow from '../../images/icons/up-arrow.png'
import Logo from '../Logo/Logo'
import { FOOTER_CONTACTS_ICONS, FOOTER_SUBSCRIBE_ICONS } from './contants'

export default function Footer() {
  return (
    <footer className='footer' id='contacts'>
      <div className='footer_content content_wrapper'>
        <div className='footer__contacts-wrapper'>
          <section className='footer__contacts'>
            <h3 className='footer_contacts-title footer-title'>Свяжитесь с нами</h3>

            <ul className='footer__contacts-list'>
              {FOOTER_CONTACTS_ICONS.map((item, i) => (
                <li className='footer__contacts-item' key={item.id}>
                  <img className='footer__contacts-icon' src={item.src} alt={item.name} />
                  {i === 3 ? (
                    <p className='footer__contacts-text'>
                      г. Москва
                      <br />
                      ул. Московская 27-35
                      <br />
                      555 555
                    </p>
                  ) : (
                    <p className='footer__contacts-text'>{item.text}</p>
                  )}
                </li>
              ))}
            </ul>
          </section>

          <section className='footer__subscribe'>
            <h3 className='footer_subscribe-title footer-title'>Подписка</h3>

            <label className='footer__form-label' htmlFor='subscription'>
              Будьте в курсе событий
              <form>
                <div className='input-wrapper'>
                  <input
                    className='footer__form-input'
                    type='email'
                    id='subscription'
                    placeholder='e-mail'
                  />
                  <button className='footer__form-button' id='button' type='submit'>
                    Отправить
                  </button>
                </div>
              </form>
            </label>

            <h3 className='footer_subscribe-title footer-title second-title '>
              Подписывайтесь на нас
            </h3>

            <div className='footer_subscribe-icons'>
              {FOOTER_SUBSCRIBE_ICONS.map((item) => (
                <a href='#/' className='footer__subscribe-link' key={item.id}>
                  <img src={item.src} alt={item.name} />
                </a>
              ))}
            </div>
          </section>
        </div>
      </div>

      <div className='line' />

      <div className='footer__bottom-wrapper'>
        <Logo />
        <img
          className='footer__up-arrow'
          src={upArrow}
          alt=''
          title='Перейти наверх страницы'
          onClick={() => {
            document.documentElement.scrollTop = 0
          }}
        />
        <span className='footer__bottom-wrapper-text'>2018 WEB</span>
      </div>
    </footer>
  )
}
