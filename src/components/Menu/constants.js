export const LIST = [
  { text: 'О нас', link: '/#about' },
  { text: 'Как это работает', link: '/#how-it-works' },
  { text: 'Отзывы', link: '/#reviews' },
  { text: 'Контакты', link: '/#contacts' },
]
