import { HashLink } from 'react-router-hash-link'
import './Menu.css'
import { LIST } from './constants'

export default function Menu() {
  return (
    <nav>
      <ul className='menu'>
        {LIST.map((i) => (
          <li className='menu__item' key={i.link}>
            <HashLink className='menu__item-text' to={i.link}>
              {i.text}
            </HashLink>
          </li>
        ))}
      </ul>
    </nav>
  )
}
